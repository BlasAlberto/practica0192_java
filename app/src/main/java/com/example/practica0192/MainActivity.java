package com.example.practica0192;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private Button btnSaludo;
    private Button btnLimpiar;
    private Button btnCerrar;
    private EditText txtNombre;
    private TextView lblSaludar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Relacionar los objetos
        btnSaludo = (Button) findViewById(R.id.btnSaludo);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        lblSaludar = (TextView) findViewById(R.id.lblSaludo);

        // Codificar el evento clic del boton saludo
        btnSaludo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Validar
                if(txtNombre.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Falto capturar la información", Toast.LENGTH_SHORT).show();
                }
                else {
                    String str = "Hola " + txtNombre.getText().toString() + ", ¿Cómo estas?";
                    lblSaludar.setText(str);
                }

            }
        });

        // Codificar el evento clic del boton limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                lblSaludar.setText(":: ::");
                txtNombre.setText("");

            }
        });

        // Codificar el evento clic del boton cerrar
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });
    }
}